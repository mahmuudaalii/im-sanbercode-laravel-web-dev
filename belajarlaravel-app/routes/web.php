<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Homecontroller;
use App\Http\Controllers\Authcontroller;
use App\Http\Controllers\castcontroller;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Homecontroller::class,'index']);

Route::get('/form', [Authcontroller::class,'menu']);

Route::post('/kirim', [Authcontroller::class,'halaman']);

Route::get('/table',function(){
    return view('form.table');
} );

Route::get('/data-table',function(){
    return view('form.data-table');
} );

//CRUD

//create Data
//route mengarah ke form tambah cast
Route::get('/cast/create', [castcontroller::class, 'create']);
//Route untuk menyimpan inputan kedalam database table cast
Route::post('/cast', [castcontroller::class, 'store']);

//Read Data
//Rute mengarah ke halaman tampil semua di data table cast
Route::get('/cast', [castcontroller::class, 'index']);
//Route Detail cast berdasarkan id
Route::get('/cast/{id}', [castcontroller::class, 'show']);

//Update Data
//Route mengarah ke form edit cast
Route::get('/cast/{id}/edit', [castcontroller::class, 'edit']);
//Route untuk edit data berdasarkan id cast
Route::put('/cast/{id}', [castcontroller::class, 'update']);
//Delete Data
Route::delete('/cast/{id}', [castcontroller::class, 'destroy']);

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castcontroller extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public  function store(request $request)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
    [
        'name.required' => "nama tidak boleh kosong harus di isi",
        'umur.required' => "umur tidak boleh kosong harus di isi",
        'bio.required' => "bio tidak boleh kosong harus di isi",
    ]);
        DB::table('cast')->insert([
            'name' => $request['name'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
            
        ]);
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        // dd($cast);
 
        return view('cast.tampil', ['cast' => $cast]);
    }
    public function show($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' => $cast]);
    }
    public function edit($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($id, request $request){
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
    [
        'name.required' => "nama tidak boleh kosong harus di isi",
        'umur.required' => "umur tidak boleh kosong harus di isi",
        'bio.required' => "bio tidak boleh kosong harus di isi",
    ]);
    DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'name' => $request['name'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]);
                return redirect('/cast');
    }
    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}

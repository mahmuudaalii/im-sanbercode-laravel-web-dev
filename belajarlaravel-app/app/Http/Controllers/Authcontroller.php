<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Authcontroller extends Controller
{
    public function menu()
    {
        return view('form.menu');
    }

    public function halaman(request $request)
    {
        // dd($request->all());
        $dname = $request['dname'];
        $bname = $request['bname'];
        $cars = $request['cars'];
        $skill = $request['skill'];
        $message = $request['message'];

        return view('form.halaman',['dname'=>$dname , 'bname'=>$bname , 'cars'=>$cars , 'skill'=>$skill , 'message'=>$message]);
    }
}

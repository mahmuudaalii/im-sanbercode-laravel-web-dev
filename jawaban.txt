1 Buat Database
CREATE DATABASE myshop;

2 a.Buat Table
  users
CREATE TABLE users( id int(8) AUTO_INCREMENT PRIMARY KEY, nama varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

  b.Buat Table
  categories
CREATE TABLE categories( id int(8) AUTO_INCREMENT PRIMARY KEY, nama varchar(255) NOT null );

  c.Buat Table
  items
CREATE TABLE items( id int(8) AUTO_INCREMENT PRIMARY KEY, nama varchar(255) NOT null, description varchar(255) NOT null, price int(15) NOT null, stock int(10) NOT null, categories_id int(8) NOT null, FOREIGN KEY(categories_id) REFERENCES categories(id) );

3 a.insert data
  users
  INSERT INTO users(nama, email, password) VALUES("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

  b.insert data
  categories
INSERT INTO categories(nama) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded");

  c.insert data
  items
INSERT INTO items(nama, description, price, stock, categories_id) VALUES("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4 get data
  a.SELECT id, nama, email FROM users;

  b.1-SELECT * FROM items WHERE price > 1000000;
  b.2-SELECT * FROM items WHERE nama LIKE "%Watch";
  b.3-SELECT * FROM items WHERE nama LIKE "sum%";
  b.3-SELECT * FROM items WHERE nama LIKE "%kl%";

  c.SELECT items.*, categories.nama as categories FROM items INNER JOIN categories ON items.categories_id = categories.id;

5 UPDATE items set price=2500000 WHERE id=1;
